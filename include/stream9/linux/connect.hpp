#ifndef STREAM9_LINUX_CONNECT_HPP
#define STREAM9_LINUX_CONNECT_HPP

#include "addrinfo.hpp"
#include "socket.hpp"

#include <stream9/linux/error.hpp>
#include <stream9/outcome.hpp>

#include <sys/socket.h>

namespace stream9::linux {

inline void
connect(socket_ref sock, struct ::sockaddr const& addr, ::socklen_t addrlen)
{
    auto rc = ::connect(sock, &addr, addrlen);
    if (rc == -1) {
        throw error {
            "connect(2)", lx::make_error_code(errno), {
                { "sock", sock }, //TODO addr
            }
        };
    }
}

inline void
connect(socket_ref sock, addrinfo const& ai)
{
    return connect(sock, ai.addr(), ai.addrlen());
}

inline outcome<void, errc>
connect_nothrow(socket_ref sock, struct ::sockaddr const& addr, ::socklen_t addrlen)
{
    auto rc = ::connect(sock, &addr, addrlen);
    if (rc == -1) {
        return static_cast<errc>(errno);
    }
    else {
        return errc {};
    }
}

inline outcome<void, errc>
connect_nothrow(socket_ref sock, addrinfo const& ai)
{
    return connect_nothrow(sock, ai.addr(), ai.addrlen());
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_CONNECT_HPP
