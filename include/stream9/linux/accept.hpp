#ifndef STREAM9_LINUX_ACCEPT_HPP
#define STREAM9_LINUX_ACCEPT_HPP

#include "socket.hpp"

#include <stream9/linux/error.hpp>

#include <sys/socket.h>

namespace stream9::linux {

inline socket
accept(socket_ref sock, struct ::sockaddr& addr, ::socklen_t& addrlen)
{
    while (true) {
        auto rc = ::accept(sock, &addr, &addrlen);
        if (rc == -1) {
            if (errno == EINTR) continue;
            if (errno == ECONNABORTED) continue;
            else {
                throw error {
                    "accept(2)",
                    lx::make_error_code(errno),
                };
            }
        }
        else {
            return socket { rc };
        }
    }
}

inline socket
accept(socket_ref sock)
{
    while (true) {
        auto rc = ::accept(sock, nullptr, nullptr);
        if (rc == -1) {
            if (errno == EINTR) continue;
            if (errno == ECONNABORTED) continue;
            else {
                throw error {
                    "accept(2)",
                    lx::make_error_code(errno),
                };
            }
        }
        else {
            return socket { rc };
        }
    }
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_ACCEPT_HPP
