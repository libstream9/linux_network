#ifndef STREAM9_LINUX_SETSOCKOPT_HPP
#define STREAM9_LINUX_SETSOCKOPT_HPP

#include "socket.hpp"

#include <stream9/linux/error.hpp>

namespace stream9::linux {

template<typename T>
void
setsockopt(socket_ref sock,
           int level,
           int optname,
           T const& optval)
{
    auto rc = ::setsockopt(sock, level, optname, &optval, sizeof(optval));
    if (rc == -1) {
        throw error {
            "setsockopt(2)",
            lx::make_error_code(errno), {
                { "sockfd", sock },
                { "optname", optname }, //TODO to symbol
            }
        };
    }
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_SETSOCKOPT_HPP
