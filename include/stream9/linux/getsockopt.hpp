#ifndef STREAM9_LINUX_GETSOCKOPT_HPP
#define STREAM9_LINUX_GETSOCKOPT_HPP

#include "socket.hpp"

#include <stream9/linux/error.hpp>

#include <sys/socket.h>

namespace stream9::linux {

template<typename T>
::socklen_t
getsockopt(socket_ref sock,
           int level,
           int optname,
           T& optval)
{
    ::socklen_t optlen = sizeof(T);
    auto rc = ::getsockopt(sock, level, optname, &optval, &optlen);
    if (rc == -1) {
        throw error {
            "getsockopt(2)",
            lx::make_error_code(errno), {
                { "sockfd", sock },
                { "optname", optname }, //TODO to symbol
            }
        };
    }
    else {
        return optlen;
    }
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_GETSOCKOPT_HPP
