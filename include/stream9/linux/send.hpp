#ifndef STREAM9_LINUX_SEND_HPP
#define STREAM9_LINUX_SEND_HPP

#include "socket.hpp"

#include <sys/socket.h>

#include <stream9/array_view.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/number.hpp>

namespace stream9::linux {

inline outcome<natural<::ssize_t>, errc>
send(socket_ref sockfd,
     void const* buf, ::size_t len,
     int flags = 0) noexcept
{
    auto n = ::send(sockfd, buf, len, flags);
    if (n == -1) {
        return { st9::error_tag(), static_cast<errc>(errno) };
    }
    else {
        return n;
    }
}

inline outcome<natural<::ssize_t>, errc>
send(socket_ref sockfd,
     array_view<char const> buf,
     int flags = 0) noexcept
{
    return lx::send(sockfd, buf.data(), buf.size(), flags);
}

inline outcome<natural<::ssize_t>, errc>
sendto(socket_ref sockfd,
       void const* buf, ::size_t len,
       int flags,
       struct ::sockaddr const& dest_addr, ::socklen_t addrlen) noexcept
{
    auto n = ::sendto(sockfd, buf, len, flags, &dest_addr, addrlen);
    if (n == -1) {
        return { st9::error_tag(), static_cast<errc>(errno) };
    }
    else {
        return n;
    }
}

inline outcome<natural<::ssize_t>, errc>
sendto(socket_ref sockfd,
       array_view<char const> buf,
       int flags,
       struct ::sockaddr const& dest_addr, ::socklen_t addrlen) noexcept
{
    return lx::sendto(sockfd, buf.data(), buf.size(), flags, dest_addr, addrlen);
}

inline outcome<natural<::ssize_t>, errc>
sendto(socket_ref sockfd,
       array_view<char const> buf,
       int flags,
       struct ::sockaddr_in const& dest_addr) noexcept
{
    return lx::sendto(sockfd, buf.data(), buf.size(), flags,
             reinterpret_cast<::sockaddr const&>(dest_addr), sizeof(dest_addr));
}

inline outcome<natural<::ssize_t>, errc>
sendto(socket_ref sockfd,
       array_view<char const> buf,
       int flags,
       struct ::sockaddr_un const& dest_addr) noexcept
{
    return lx::sendto(sockfd, buf.data(), buf.size(), flags,
             reinterpret_cast<::sockaddr const&>(dest_addr), sizeof(dest_addr));
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_SEND_HPP
