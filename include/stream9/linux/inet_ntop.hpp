#ifndef STREAM9_LINUX_INET_NTOP_HPP
#define STREAM9_LINUX_INET_NTOP_HPP

#include <stream9/string.hpp>
#include <stream9/linux/error.hpp>

#include <arpa/inet.h>

namespace stream9::linux {

string
inet_ntop(struct ::in_addr const&);

string
inet_ntop(struct ::in6_addr const&);

} // namespace stream9::linux

#endif // STREAM9_LINUX_INET_NTOP_HPP
