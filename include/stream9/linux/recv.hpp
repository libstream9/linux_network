#ifndef STREAM9_LINUX_RECV_HPP
#define STREAM9_LINUX_RECV_HPP

#include "socket.hpp"

#include <sys/socket.h>
#include <sys/un.h>

#include <stream9/array_view.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/number.hpp>

namespace stream9::linux {

inline outcome<natural<::ssize_t>, errc>
recv(socket_ref sockfd,
     void* buf, ::size_t len,
     int flags = 0) noexcept
{
    auto n = ::recv(sockfd, buf, len, flags);
    if (n == -1) {
        return { st9::error_tag(), static_cast<errc>(errno) };
    }
    else {
        return n;
    }
}

inline outcome<natural<::ssize_t>, errc>
recv(socket_ref sockfd,
     array_view<char> buf,
     int flags = 0) noexcept
{
    return recv(sockfd, buf.data(), buf.size(), flags);
}

inline outcome<natural<::ssize_t>, errc>
recvfrom(socket_ref sockfd,
         void* buf, ::size_t len,
         int flags,
         struct ::sockaddr& addr, ::socklen_t& addrlen) noexcept
{
    auto n = ::recvfrom(sockfd, buf, len, flags, &addr, &addrlen);
    if (n == -1) {
        return { st9::error_tag(), static_cast<errc>(errno) };
    }
    else {
        assert(n >= 0);
        return n;
    }
}

inline outcome<natural<::ssize_t>, errc>
recvfrom(socket_ref sockfd,
         array_view<char> buf,
         int flags,
         struct ::sockaddr& addr, ::socklen_t& addrlen) noexcept
{
    return lx::recvfrom(sockfd, buf.data(), buf.size(), flags, addr, addrlen);
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_RECV_HPP
