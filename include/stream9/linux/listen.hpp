#ifndef STREAM9_LINUX_LISTEN_HPP
#define STREAM9_LINUX_LISTEN_HPP

#include "socket.hpp"

#include <stream9/linux/error.hpp>

#include <sys/socket.h>

namespace stream9::linux {

inline void
listen(socket_ref sock, int backlog)
{
    auto rc = ::listen(sock, backlog);
    if (rc == -1) {
        throw error {
            "listen(2)",
            lx::make_error_code(errno), {
                { "sockfd", sock },
                { "backlog", backlog },
            }
        };
    }
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_LISTEN_HPP
