#ifndef STREAM9_LINUX_BIND_HPP
#define STREAM9_LINUX_BIND_HPP

#include "addrinfo.hpp"
#include "socket.hpp"
#include "sockaddr.hpp"

#include <stream9/linux/error.hpp>

#include <sys/socket.h>

namespace stream9::linux {

inline void
bind(socket_ref sock, struct ::sockaddr const& addr, ::socklen_t addrlen)
{
    auto rc = ::bind(sock, &addr, addrlen);
    if (rc == -1) {
        throw error {
            "bind(2)",
            lx::make_error_code(errno), {
                { "sockfd", sock },
                //TODO addr
            }
        };
    }
}

inline void
bind(socket_ref sock, addrinfo const& ai)
{
    auto rc = ::bind(sock, &ai.addr(), ai.addrlen());
    if (rc == -1) {
        throw error {
            "bind(2)",
            lx::make_error_code(errno), {
                { "sockfd", sock },
                //TODO addr
            }
        };
    }
}

inline void
bind(socket_ref sock, struct ::sockaddr_un const& addr)
{
    bind(sock, reinterpret_cast<struct ::sockaddr const&>(addr), sizeof(addr));
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_BIND_HPP
