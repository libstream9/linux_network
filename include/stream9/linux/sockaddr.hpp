#ifndef STREAM9_LINUX_SOCKADDR_HPP
#define STREAM9_LINUX_SOCKADDR_HPP

#include <stream9/cstring_ptr.hpp>

#include <sys/socket.h>
#include <sys/un.h>

namespace stream9::linux {

struct sockaddr_in : ::sockaddr_in
{
    sockaddr_in(in_addr_t addr, in_port_t port) noexcept
    {
        bzero(this, sizeof(*this));
        this->sin_family = AF_INET;
        this->sin_addr.s_addr = htonl(addr);
        this->sin_port = htons(port);
    }

    operator struct ::sockaddr& () noexcept
    {
        return reinterpret_cast<struct ::sockaddr&>(*this);
    }

    operator struct ::sockaddr const& () const noexcept
    {
        return reinterpret_cast<struct ::sockaddr const&>(*this);
    }
};

struct sockaddr_un : ::sockaddr_un
{
    sockaddr_un(cstring_ptr path) noexcept
    {
        this->sun_family = AF_UNIX;
        ::strncpy(this->sun_path, path, sizeof(this->sun_path)-1);
    }

    operator struct ::sockaddr& () noexcept
    {
        return reinterpret_cast<struct ::sockaddr&>(*this);
    }

    operator struct ::sockaddr const& () const noexcept
    {
        return reinterpret_cast<struct ::sockaddr const&>(*this);
    }
};

struct sockaddr_storage : ::sockaddr_storage
{
    operator struct ::sockaddr& () noexcept
    {
        return reinterpret_cast<struct ::sockaddr&>(*this);
    }

    operator struct ::sockaddr const& () const noexcept
    {
        return reinterpret_cast<struct ::sockaddr const&>(*this);
    }
};

} // namespace stream9::linux

#endif // STREAM9_LINUX_SOCKADDR_HPP
