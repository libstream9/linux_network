#ifndef STREAM9_LINUX_ADDRINFO_HPP
#define STREAM9_LINUX_ADDRINFO_HPP

#include "network/symbol.hpp"

#include <stream9/cstring_ptr.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/optional.hpp>
#include <stream9/outcome.hpp>

#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <iterator>
#include <ostream>
#include <system_error>

namespace stream9::linux {

class addrinfo_iterator;

enum class address_type { tcp, udp, tcp4, tcp6, udp4, udp6 };
using enum address_type;

/*
 * @model is_nothrow_move_constructible
 * @model is_nothrow_move_assignable
 * @model is_nothrow_swappable
 * @model is_nothrow_destructible
 */
class addrinfo
{
public:
    using iterator = addrinfo_iterator;

public:
    // essential
    addrinfo(char const* node, char const* service, ::addrinfo const* hints);

    addrinfo(cstring_ptr node, address_type = tcp, int flags = 0);

    addrinfo(opt<cstring_ptr> node, opt<cstring_ptr> service, address_type = tcp, int flags = 0);

    explicit addrinfo(struct ::addrinfo& v) noexcept;

    addrinfo(addrinfo const&) = delete;
    addrinfo& operator=(addrinfo const&) = delete;

    addrinfo(addrinfo&&) noexcept;
    addrinfo& operator=(addrinfo&&) noexcept;

    ~addrinfo() noexcept;

    // conversion
    operator struct ::addrinfo const& () const noexcept;

    // accessor
    int family() const noexcept;
    int socktype() const noexcept;
    int protocol() const noexcept;
    struct ::sockaddr& addr() const noexcept;
    ::socklen_t addrlen() const noexcept;
    char* canonname() const noexcept;
    int flags() const noexcept;

    iterator begin() const noexcept; // forward_iterator of addrinfo_ref
    std::default_sentinel_t end() const noexcept;

    // error
    static std::error_category const& error_category() noexcept;

private:
    struct ::addrinfo* m_v; // non-null
};

class addrinfo_ref
{
public:
    using iterator = addrinfo_iterator;

public:
    // essential
    addrinfo_ref(addrinfo const&) noexcept;
    addrinfo_ref(struct ::addrinfo const&) noexcept;

    addrinfo_ref(addrinfo_ref const&) = default;
    addrinfo_ref& operator=(addrinfo_ref const&) = default;

    addrinfo_ref(addrinfo_ref&&) = default;
    addrinfo_ref& operator=(addrinfo_ref&&) = default;

    ~addrinfo_ref() = default;

    // conversion
    operator struct ::addrinfo const& () const noexcept;

    // accessor
    int family() const noexcept;
    int socktype() const noexcept;
    int protocol() const noexcept;
    struct ::sockaddr const& addr() const noexcept;
    ::socklen_t addrlen() const noexcept;
    char* canonname() const noexcept;
    int flags() const noexcept;

    iterator begin() const noexcept; // forward_iterator of addrinfo_ref
    std::default_sentinel_t end() const noexcept;

private:
    struct ::addrinfo const* m_v; // non-null
};

} // namespace stream9::linux

namespace stream9::json {

void tag_invoke(value_from_tag, json::value&, struct ::sockaddr const&);
void tag_invoke(value_from_tag, json::value&, struct ::addrinfo const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_ADDRINFO_HPP

#include "network/addrinfo.ipp"
