#ifndef STREAM9_LINUX_NETWORK_ADDRINFO_IPP
#define STREAM9_LINUX_NETWORK_ADDRINFO_IPP

#include <cassert>
#include <utility>

#include <stream9/iterator_facade.hpp>

namespace stream9::linux {

class addrinfo_iterator : public iterator_facade<addrinfo_iterator,
                                    std::forward_iterator_tag,
                                    addrinfo_ref >
{
public:
    addrinfo_iterator() = default; // partially-formed

    addrinfo_iterator(struct ::addrinfo const* v)
        : m_v { v }
    {}

private:
    friend class stream9::iterator_core_access;

    addrinfo_ref
    dereference() const noexcept
    {
        return *m_v;
    }

    void
    increment() noexcept
    {
        m_v = m_v->ai_next;
    }

    bool
    equal(addrinfo_iterator const& o) const noexcept
    {
        return m_v == o.m_v;
    }

    bool
    equal(std::default_sentinel_t const&) const noexcept
    {
        return m_v == nullptr;
    }

private:
    struct ::addrinfo const* m_v;
};

/*
 * class addrinfo
 */
inline addrinfo::
addrinfo(struct ::addrinfo& v) noexcept
    : m_v { &v }
{
    assert(m_v);
}

inline addrinfo::
addrinfo(addrinfo&& o) noexcept
{
    m_v = o.m_v;
    o.m_v = nullptr;
}

inline addrinfo& addrinfo::
operator=(addrinfo&& o) noexcept
{
    using std::swap;
    swap(m_v, o.m_v);
    return *this;
}

inline addrinfo::
~addrinfo() noexcept
{
    if (m_v) {
        ::freeaddrinfo(m_v);
    }
}

inline addrinfo::
operator struct ::addrinfo const& () const noexcept
{
    return *m_v;
}

inline int addrinfo::
family() const noexcept
{
    return m_v->ai_family;
}

inline int addrinfo::
socktype() const noexcept
{
    return m_v->ai_socktype;
}

inline int addrinfo::
protocol() const noexcept
{
    return m_v->ai_protocol;
}

inline struct ::sockaddr& addrinfo::
addr() const noexcept
{
    return *m_v->ai_addr;
}

inline ::socklen_t addrinfo::
addrlen() const noexcept
{
    return m_v->ai_addrlen;
}

inline char* addrinfo::
canonname() const noexcept
{
    return m_v->ai_canonname;
}

inline int addrinfo::
flags() const noexcept
{
    return m_v->ai_flags;
}

inline addrinfo::iterator addrinfo::
begin() const noexcept
{
    return m_v;
}

inline std::default_sentinel_t addrinfo::
end() const noexcept
{
    return {};
}

/*
 * class addrinfo_ref
 */
inline addrinfo_ref::
addrinfo_ref(addrinfo const& v) noexcept
    : m_v { &static_cast<struct ::addrinfo const&>(v) }
{}

inline addrinfo_ref::
addrinfo_ref(struct ::addrinfo const& v) noexcept
    : m_v { &v }
{}

inline addrinfo_ref::
operator struct ::addrinfo const& () const noexcept
{
    return *m_v;
}

inline int addrinfo_ref::
family() const noexcept
{
    return m_v->ai_family;
}

inline int addrinfo_ref::
socktype() const noexcept
{
    return m_v->ai_socktype;
}

inline int addrinfo_ref::
protocol() const noexcept
{
    return m_v->ai_protocol;
}

inline struct ::sockaddr const& addrinfo_ref::
addr() const noexcept
{
    return *m_v->ai_addr;
}

inline ::socklen_t addrinfo_ref::
addrlen() const noexcept
{
    return m_v->ai_addrlen;
}

inline char* addrinfo_ref::
canonname() const noexcept
{
    return m_v->ai_canonname;
}

inline int addrinfo_ref::
flags() const noexcept
{
    return m_v->ai_flags;
}

inline addrinfo_ref::iterator addrinfo_ref::
begin() const noexcept
{
    return m_v;
}

inline std::default_sentinel_t addrinfo_ref::
end() const noexcept
{
    return {};
}

/*
 * free function
 */
inline std::ostream&
operator<<(std::ostream& os, addrinfo const& ai)
{
    return os << json::value_from(ai);
}

inline std::ostream&
operator<<(std::ostream& os, addrinfo_ref const& ai)
{
    return os << json::value_from(ai);
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_NETWORK_ADDRINFO_IPP
