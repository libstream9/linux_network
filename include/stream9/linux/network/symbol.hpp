#ifndef STREAM9_LINUX_NETWORK_SYMBOL_HPP
#define STREAM9_LINUX_NETWORK_SYMBOL_HPP

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::linux {

string_view
address_family_to_symbol(int);

string
socktype_to_symbol(int);

string
addrinfo_flags_to_symbol(int);

string
protocol_to_symbol(int);

string
addrinfo_error_to_symbol(int);

} // namespace stream9::linux

#endif // STREAM9_LINUX_NETWORK_SYMBOL_HPP
