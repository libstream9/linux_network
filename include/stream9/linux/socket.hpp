#ifndef STREAM9_LINUX_SOCKET_HPP
#define STREAM9_LINUX_SOCKET_HPP

#include "addrinfo.hpp"

#include <stream9/linux/fd.hpp>
#include <stream9/linux/error.hpp>

#include <sys/socket.h>

namespace stream9::linux {

class socket : public fd
{
public:
    // essentials
    socket(int domain, int type, int protocol);

    socket(addrinfo_ref ai, int options = 0)
        : socket { ai.family(), ai.socktype() | options, ai.protocol() }
    {}

    explicit socket(safe_integer<int, 0> fd_)
        : fd { fd_ }
    {}

    ~socket() noexcept;

    socket(socket const&) = delete;
    socket& operator=(socket const&) = delete;
    socket(socket&&) = default;
    socket& operator=(socket&&) = default;

    // query
    int socket_error() const; // (getsockopt SO_ERROR)
    int input_available() const; // (ioctl FIONREAD)
    int input_buffer_size() const; // (getsockopt SO_RCVBUF)
    int output_available() const; // (ioctl TIOCOUTQ)
    int output_buffer_size() const; // (getsockopt SO_SNDBUF)

    // modifier
    void set_reuse_address(bool); // (setsockopt SO_REUSEADDR)

    // command
    void shutdown(int how); // SHUT_RD or SHUT_WR or SHUT_RDWR
};

class socket_ref : public fd_ref
{
public:
    // essentials
    socket_ref(socket const& s)
        : fd_ref { s }
    {}

    socket_ref(int fd)
        : fd_ref { fd }
    {}

    // query
    int socket_error() const; // (getsockopt SO_ERROR)
    int input_available() const; // (ioctl FIONREAD)
    int input_buffer_size() const; // (getsockopt SO_RCVBUF)
    int output_available() const; // (ioctl TIOCOUTQ)
    int output_buffer_size() const; // (getsockopt SO_SNDBUF)

    // modifier
    void set_reuse_address(bool); // (setsockopt SO_REUSEADDR)

    // command
    void shutdown(int how); // SHUT_RD or SHUT_WR or SHUT_RDWR
};

} // namespace stream9::linux

#endif // STREAM9_LINUX_SOCKET_HPP
