#include <stream9/linux/socket.hpp>

#include <stream9/linux/network/symbol.hpp>
#include <stream9/log.hpp>

#include <sys/ioctl.h>

namespace stream9::linux {

static fd
make_socket(int domain, int type, int protocol)
{
    auto rc = ::socket(domain, type, protocol);
    if (rc == -1) {
        throw error {
            "socket(2)",
            lx::make_error_code(errno), {
                { "domain", address_family_to_symbol(domain) },
                { "type", socktype_to_symbol(type) },
                { "protocol", protocol_to_symbol(protocol) },
            }
        };
    }

    return fd { rc };
}

static
outcome<int, lx::errc>
do_ioctl(int fd, int op)
{
    int n;
    auto rc = ::ioctl(fd, op, &n);
    if (rc == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return n;
    }
}

static
outcome<int, lx::errc>
do_getsockopt(int fd, int level, int op)
{
    int n;
    ::socklen_t len = sizeof(n);
    auto rc = ::getsockopt(fd, level, op, &n, &len);
    if (rc == -1) {
        return { st9::error_tag(), static_cast<lx::errc>(errno) };
    }
    else {
        return n;
    }
}

static string_view
how_to_symbol(int how)
{
    switch (how) {
        case SHUT_RD: return "SHUT_RD";
        case SHUT_WR: return "SHUT_WR";
        case SHUT_RDWR: return "SHUT_RDWR";
        default: return "unknown";
    }
}

/*
 * class socket
 */
socket::
socket(int domain, int type, int protocol)
    : fd { make_socket(domain, type, protocol) }
{}

socket::
~socket() noexcept
{
    if (*this == -1) return;

    int rc = ::shutdown(*this, SHUT_RDWR);
    if (rc == -1) {
        if (errno == ENOTCONN) {}
        else {
            auto ec = lx::make_error_code(errno);
            log::err() << "fail to shutdown a socket on destruction:" << ec.message();
        }
    }
}

int socket::
socket_error() const
{
    int e = 0;
    ::socklen_t len = sizeof(e);
    auto rc = ::getsockopt(*this, SOL_SOCKET, SO_ERROR, &e, &len);
    if (rc == -1) {
        throw error {
            "getsockopt(2)",
            lx::make_error_code(errno),
        };
    }
    else {
        return e;
    }
}

int socket::
input_available() const
{
    return do_ioctl(*this, FIONREAD).or_throw([](auto e) {
        return error { "ioctl(2)", e };
    });
}

int socket::
input_buffer_size() const
{
    return do_getsockopt(*this, SOL_SOCKET, SO_RCVBUF).or_throw([](auto e) {
        return error { "getsockopt(2)", e };
    });
}

int socket::
output_available() const
{
    return do_ioctl(*this, TIOCOUTQ).or_throw([](auto e) {
        return error { "ioctl(2)", e };
    });
}

int socket::
output_buffer_size() const
{
    return do_getsockopt(*this, SOL_SOCKET, SO_SNDBUF).or_throw([](auto e) {
        return error { "getsockopt(2)", e };
    });
}

void socket::
set_reuse_address(bool on)
{
    int v = on ? 1 : 0;
    auto rc = ::setsockopt(*this, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v));
    if (rc == -1) {
        throw error {
            "setsockopt(2)",
            lx::make_error_code(errno),
        };
    }
}

void socket::
shutdown(int how)
{
    if (*this == -1) return;

    auto rc = ::shutdown(*this, how);
    if (rc == -1) {
        if (errno == ENOTCONN) {}
        else {
            throw error {
                "shutdown(2)",
                lx::make_error_code(errno), {
                    { "how", how_to_symbol(how) }
                }
            };
        }
    }
}

/*
 * class socket_ref
 */
int socket_ref::
input_available() const
{
    return do_ioctl(*this, FIONREAD).or_throw([](auto e) {
        return error { "ioctl(2)", e };
    });
}

int socket_ref::
input_buffer_size() const
{
    return do_getsockopt(*this, SOL_SOCKET, SO_RCVBUF).or_throw([](auto e) {
        return error { "getsockopt(2)", e };
    });
}

int socket_ref::
output_available() const
{
    return do_ioctl(*this, TIOCOUTQ).or_throw([](auto e) {
        return error { "ioctl(2)", e };
    });
}

int socket_ref::
output_buffer_size() const
{
    return do_getsockopt(*this, SOL_SOCKET, SO_SNDBUF).or_throw([](auto e) {
        return error { "getsockopt(2)", e };
    });
}

void socket_ref::
set_reuse_address(bool on)
{
    int v = on ? 1 : 0;
    auto rc = ::setsockopt(*this, SOL_SOCKET, SO_REUSEADDR, &v, sizeof(v));
    if (rc == -1) {
        throw error {
            "setsockopt(2)",
            lx::make_error_code(errno),
        };
    }
}

void socket_ref::
shutdown(int how)
{
    if (*this == -1) return;

    auto rc = ::shutdown(*this, how);
    if (rc == -1) {
        if (errno == ENOTCONN) {}
        else {
            throw error {
                "shutdown(2)",
                lx::make_error_code(errno), {
                    { "how", how_to_symbol(how) }
                }
            };
        }
    }
}

} // namespace stream9::linux
