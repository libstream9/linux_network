#include <stream9/linux/network/symbol.hpp>
#include <stream9/bits/to_symbol.hpp>

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

namespace stream9::linux {

string_view
address_family_to_symbol(int af)
{
#define ENTRY(x) case x: return #x
    switch (af) {
        ENTRY(AF_UNSPEC);
        ENTRY(AF_LOCAL);
        //ENTRY(AF_UNIX); // same as AF_LOCAL
        //ENTRY(AF_FILE); // same as AF_LOCAL
        ENTRY(AF_INET);
        ENTRY(AF_AX25);
        ENTRY(AF_IPX);
        ENTRY(AF_APPLETALK);
        ENTRY(AF_NETROM);
        ENTRY(AF_BRIDGE);
        ENTRY(AF_ATMPVC);
        ENTRY(AF_X25);
        ENTRY(AF_INET6);
        ENTRY(AF_ROSE);
        ENTRY(AF_DECnet);
        ENTRY(AF_NETBEUI);
        ENTRY(AF_SECURITY);
        ENTRY(AF_KEY);
        ENTRY(AF_NETLINK);
        //ENTRY(AF_ROUTE); // same as AF_NETLINK
        ENTRY(AF_PACKET);
        ENTRY(AF_ASH);
        ENTRY(AF_ECONET);
        ENTRY(AF_ATMSVC);
        ENTRY(AF_RDS);
        ENTRY(AF_SNA);
        ENTRY(AF_IRDA);
        ENTRY(AF_PPPOX);
        ENTRY(AF_WANPIPE);
        ENTRY(AF_LLC);
        ENTRY(AF_IB);
        ENTRY(AF_MPLS);
        ENTRY(AF_CAN);
        ENTRY(AF_TIPC);
        ENTRY(AF_BLUETOOTH);
        ENTRY(AF_IUCV);
        ENTRY(AF_RXRPC);
        ENTRY(AF_ISDN);
        ENTRY(AF_PHONET);
        ENTRY(AF_IEEE802154);
        ENTRY(AF_CAIF);
        ENTRY(AF_ALG);
        ENTRY(AF_NFC);
        ENTRY(AF_VSOCK);
        ENTRY(AF_KCM);
        ENTRY(AF_QIPCRTR);
        ENTRY(AF_SMC);
        ENTRY(AF_XDP);
        ENTRY(AF_MCTP);
        ENTRY(AF_MAX);
    }
    return "unknown address family";
#undef ENTRY
}

string
socktype_to_symbol(int v)
{
    string rv;

    auto v_ = v & ~(SOCK_CLOEXEC | SOCK_NONBLOCK);
    switch (v_) {
        case SOCK_STREAM:
            rv = "SOCK_STREAM";
            break;
        case SOCK_DGRAM:
            rv = "SOCK_DGRAM";
            break;
        case SOCK_RAW:
            rv = "SOCK_RAW";
            break;
        case SOCK_RDM:
            rv = "SOCK_RDM";
            break;
        case SOCK_SEQPACKET:
            rv = "SOCK_SEQPACKET";
            break;
        case SOCK_DCCP:
            rv = "SOCK_DCCP";
            break;
        case SOCK_PACKET:
            rv = "SOCK_PACKET";
            break;
        default:
            rv = "unknown socket type";
            break;
    }

    if ((v & SOCK_CLOEXEC) != 0) {
        rv += " | SOCK_CLOEXEC";
    }
    if ((v & SOCK_NONBLOCK) != 0) {
        rv += " | SOCK_NONBLOCK";
    }

    return rv;
}

string
addrinfo_flags_to_symbol(int v)
{
    return bits::ored_flags_to_symbol(v, {
        STREAM9_FLAG_ENTRY(AI_PASSIVE),
        STREAM9_FLAG_ENTRY(AI_CANONNAME),
        STREAM9_FLAG_ENTRY(AI_NUMERICHOST),
        STREAM9_FLAG_ENTRY(AI_V4MAPPED),
        STREAM9_FLAG_ENTRY(AI_ALL),
        STREAM9_FLAG_ENTRY(AI_ADDRCONFIG),
        STREAM9_FLAG_ENTRY(AI_NUMERICSERV),
    });
}

string
protocol_to_symbol(int v)
{
#define ENTRY(x) case x: return #x; break;
    switch (v) {
        ENTRY(IPPROTO_IP)       /* Dummy protocol for TCP		*/
        ENTRY(IPPROTO_ICMP)		/* Internet Control Message Protocol	*/
        ENTRY(IPPROTO_IGMP)     /* Internet Group Management Protocol	*/
        ENTRY(IPPROTO_IPIP)		/* IPIP tunnels (older KA9Q tunnels use 94) */
        ENTRY(IPPROTO_TCP)		/* Transmission Control Protocol	*/
        ENTRY(IPPROTO_EGP)		/* Exterior Gateway Protocol		*/
        ENTRY(IPPROTO_PUP)		/* PUP protocol				*/
        ENTRY(IPPROTO_UDP)		/* User Datagram Protocol		*/
        ENTRY(IPPROTO_IDP)		/* XNS IDP protocol			*/
        ENTRY(IPPROTO_TP)		/* SO Transport Protocol Class 4	*/
        ENTRY(IPPROTO_DCCP)		/* Datagram Congestion Control Protocol */
        ENTRY(IPPROTO_IPV6)		/* IPv6-in-IPv4 tunnelling		*/
        ENTRY(IPPROTO_RSVP)		/* RSVP Protocol			*/
        ENTRY(IPPROTO_GRE)		/* Cisco GRE tunnels (rfc 1701,1702)	*/
        ENTRY(IPPROTO_ESP)		/* Encapsulation Security Payload protocol */
        ENTRY(IPPROTO_AH)		/* Authentication Header protocol	*/
        ENTRY(IPPROTO_MTP)		/* Multicast Transport Protocol		*/
        ENTRY(IPPROTO_BEETPH)		/* IP option pseudo header for BEET	*/
        ENTRY(IPPROTO_ENCAP)		/* Encapsulation Header			*/
        ENTRY(IPPROTO_PIM)		/* Protocol Independent Multicast	*/
        ENTRY(IPPROTO_COMP)		/* Compression Header Protocol		*/
        ENTRY(IPPROTO_L2TP)		/* Layer 2 Tunnelling Protocol		*/
        ENTRY(IPPROTO_SCTP)		/* Stream Control Transport Protocol	*/
        ENTRY(IPPROTO_UDPLITE)	/* UDP-Lite (RFC 3828)			*/
        ENTRY(IPPROTO_MPLS)		/* MPLS in IP (RFC 4023)		*/
        ENTRY(IPPROTO_ETHERNET)	/* Ethernet-within-IPv6 Encapsulation	*/
        ENTRY(IPPROTO_RAW)		/* Raw IP packets			*/
        ENTRY(IPPROTO_MPTCP)		/* Multipath TCP connection		*/
#undef ENTRY
        default:
            return "unknown protocol";
    }
}

string
addrinfo_error_to_symbol(int ec)
{
    string rv;
#define ENTRY(x) case x: rv = #x; break;
    switch (ec) {
        ENTRY(EAI_BADFLAGS)	 /* Invalid value for `ai_flags' field.  */
        ENTRY(EAI_NONAME)	 /* NAME or SERVICE is unknown.  */
        ENTRY(EAI_AGAIN	)    /* Temporary failure in name resolution.  */
        ENTRY(EAI_FAIL)	     /* Non-recoverable failure in name res.  */
        ENTRY(EAI_FAMILY)	 /* `ai_family' not supported.  */
        ENTRY(EAI_SOCKTYPE)	 /* `ai_socktype' not supported.  */
        ENTRY(EAI_SERVICE)	 /* SERVICE not supported for `ai_socktype'.  */
        ENTRY(EAI_MEMORY)	 /* Memory allocation failure.  */
        ENTRY(EAI_SYSTEM)	 /* System error returned in `errno'.  */
        ENTRY(EAI_OVERFLOW)  /* Argument buffer overflow.  */
#ifdef __USE_GNU
        ENTRY(EAI_NODATA)	  /* No address associated with NAME.  */
        ENTRY(EAI_ADDRFAMILY) /* Address family for NAME not supported.  */
        ENTRY(EAI_INPROGRESS) /* Processing request in progress.  */
        ENTRY(EAI_CANCELED)	  /* Request canceled.  */
        ENTRY(EAI_NOTCANCELED)/* Request not canceled.  */
        ENTRY(EAI_ALLDONE)	  /* All requests done.  */
        ENTRY(EAI_INTR)	      /* Interrupted by a signal.  */
        ENTRY(EAI_IDN_ENCODE) /* IDN encoding failed.  */
#endif
        default:
            rv = "unknown error: ";
            rv << ec;
    }
#undef ENTRY
    return rv;
}

} // namespace stream9::linux
