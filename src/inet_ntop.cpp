#include <stream9/linux/inet_ntop.hpp>

namespace stream9::linux {

string
inet_ntop(struct ::in_addr const& a)
{
    char buf[INET_ADDRSTRLEN];
    auto p = ::inet_ntop(AF_INET, &a, buf, INET_ADDRSTRLEN);
    if (p == nullptr) {
        throw error {
            "inet_ntop(3)",
            lx::make_error_code(errno),
        };
    }
    else {
        return p;
    }
}

string
inet_ntop(struct ::in6_addr const& a)
{
    char buf[INET6_ADDRSTRLEN];
    auto p = ::inet_ntop(AF_INET6, &a, buf, INET6_ADDRSTRLEN);
    if (p == nullptr) {
        throw error {
            "inet_ntop(3)",
            lx::make_error_code(errno),
        };
    }
    else {
        return p;
    }
}

} // namespace stream9::linux
