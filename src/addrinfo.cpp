#include <stream9/linux/addrinfo.hpp>

#include <stream9/linux/inet_ntop.hpp>

#include <cassert>

namespace stream9::linux {

static struct ::addrinfo
make_hints(address_type t, int flags)
{
    struct ::addrinfo h {};

    switch (t) {
        case tcp:
            h.ai_family = AF_UNSPEC;
            h.ai_socktype = SOCK_STREAM;
            h.ai_protocol = IPPROTO_TCP;
            break;
        case udp:
            h.ai_family = AF_UNSPEC;
            h.ai_socktype = SOCK_DGRAM;
            h.ai_protocol = IPPROTO_UDP;
            break;
        case tcp6:
            h.ai_family = AF_INET6;
            h.ai_socktype = SOCK_STREAM;
            h.ai_protocol = IPPROTO_TCP;
            break;
        case tcp4:
            h.ai_family = AF_INET;
            h.ai_socktype = SOCK_STREAM;
            h.ai_protocol = IPPROTO_TCP;
            break;
        case udp6:
            h.ai_family = AF_INET6;
            h.ai_socktype = SOCK_DGRAM;
            h.ai_protocol = IPPROTO_UDP;
            break;
        case udp4:
            h.ai_family = AF_INET;
            h.ai_socktype = SOCK_DGRAM;
            h.ai_protocol = IPPROTO_UDP;
            break;
    }
    h.ai_flags = flags;

    return h;
}

static struct ::addrinfo*
init(char const* node,
     char const* service,
     struct ::addrinfo const* hints)
{
    ::addrinfo* rv {};

    auto rc = ::getaddrinfo(node, service, hints, &rv);
    if (rc != 0) {
        if (rv) ::freeaddrinfo(rv);

        json::object cxt;
        if (node) { cxt["node"] = node; }
        if (service) { cxt["service"] = service; }
        if (hints) { cxt["hints"] = *hints; }
        throw error {
            "getaddrinfo(3)",
            std::error_code(rc, addrinfo::error_category()),
            std::move(cxt)
        };
    }

    return rv;
}

/*
 * addrinfo
 */
addrinfo::
addrinfo(char const* node,
         char const* service,
         ::addrinfo const* hints)
{
    m_v = init(node, service, hints);
    assert(m_v);
}

addrinfo::
addrinfo(cstring_ptr node,
         address_type t/*= tcp*/,
         int flags/*= 0*/)
{
    auto hints = make_hints(t, flags);
    m_v = init(node, nullptr, &hints);
    assert(m_v);
}

addrinfo::
addrinfo(opt<cstring_ptr> node,
         opt<cstring_ptr> service,
         address_type t/*= tcp*/,
         int flags/*= 0*/)
{
    auto hints = make_hints(t, flags);
    m_v = init(node ? node->c_str() : nullptr,
               service ? service->c_str() : nullptr,
               &hints);
    assert(m_v);
}

std::error_category const& addrinfo::
error_category() noexcept
{
    static struct impl : std::error_category {
        char const* name() const noexcept
        {
            return "stream9::linux::addrinfo";
        }

        std::string message(int ec) const
        {
            std::string msg;

            msg = addrinfo_error_to_symbol(ec);
            msg.append(": ");
            msg.append(::gai_strerror(ec));

            return msg;
        }
    } v;

    return v;
}

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(value_from_tag, json::value& jv, struct ::sockaddr const& sa)
{
    try {
        auto& obj = jv.emplace_object();
        if (sa.sa_family == AF_INET) {
            auto& sin = reinterpret_cast<struct ::sockaddr_in const&>(sa);
            obj["family"] = linux::address_family_to_symbol(sin.sin_family);
            obj["port"] = sin.sin_port;
            obj["addr"] = linux::inet_ntop(sin.sin_addr);
        }
        else if (sa.sa_family == AF_INET6) {
            auto& sin = reinterpret_cast<struct ::sockaddr_in6 const&>(sa);
            obj["family"] = linux::address_family_to_symbol(sin.sin6_family);
            obj["port"] = sin.sin6_port;
            obj["flowinfo"] = sin.sin6_flowinfo;
            obj["addr"] = linux::inet_ntop(sin.sin6_addr);
            obj["scope_id"] = sin.sin6_scope_id;
        }
        else {
            obj["family"] = linux::address_family_to_symbol(sa.sa_family);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void
tag_invoke(value_from_tag, json::value& jv, struct ::addrinfo const& ai_)
{
    try {
        auto& arr = jv.emplace_array();
        for (auto&& i: linux::addrinfo_ref(ai_)) {
            json::object obj;
            struct ::addrinfo const& ai = i;

            obj["ai_family"] = linux::address_family_to_symbol(ai.ai_family);
            if (auto v = ai.ai_socktype; v != 0) {
                obj["ai_socktype"] = linux::socktype_to_symbol(v);
            }
            if (auto v = ai.ai_protocol; v != 0) {
                obj["ai_protocol"] = linux::protocol_to_symbol(v);
            }
            if (auto v = ai.ai_addr; v != nullptr) {
                obj["ai_addr"] = json::value_from(*v);
            }
            if (auto v = ai.ai_addrlen; v != 0) {
                obj["ai_addrlen"] = v;
            }
            if (auto v = ai.ai_canonname; v != nullptr) {
                obj["ai_canonname"] = v;
            }
            if (auto v = ai.ai_flags; v != 0) {
                obj["ai_flags"] = linux::addrinfo_flags_to_symbol(v);
            }

            arr.push_back(std::move(obj));
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::json
